<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language->language ?>" xml:lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">

<head>
  <title><?php print $head_title ?></title>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

  <?php print $head ?>
  <?php print $styles ?>
  <?php print $scripts ?>

  <script type="text/javascript"><?php /* Needed to avoid Flash of Unstyle Content in IE */ ?> </script>

  <!--[if IE]>
  <link rel="stylesheet" type="text/css" href="<?php print base_path() . path_to_theme(); ?>/ie.css">
  <![endif]-->
  <!--[if lt IE 7]>
  <link rel="stylesheet" type="text/css" href="<?php print base_path() . path_to_theme(); ?>/ie6.css">
  <![endif]-->
</head>
<body>

<div id="shadow"> <!--START SHADOW-->

<div id="column"> <!--START COLUMN-->

<div id="header"> <!--START HEADER-->
  <div id="header_top"> <!--START TOP HEADER-->
    <?php if ($site_name) { ?>
      <div id="sitename"><a href="<?php print $base_path ?>" title="<?php print t('Home') ?>"><?php print $site_name ?></a></div>
    <?php } ?>
    <?php if ($site_slogan) { ?><div id="siteSlogan"><?php print $site_slogan ?></div><?php } ?>
  </div> <!--END TOP HEADER-->

<?php if ($primary_links) { ?>
<div id="primary">
  <?php print theme('links', $primary_links); ?>
</div>
<?php  } ?>
  
  <div id="header_image"> <!--START HEADER IMAGE-->
    <?php if ($header) { ?>
      <?php print $header ?>
    <?php } ?>
  </div> <!--END HEADER IMAGE-->

  <div class="clean">&nbsp;</div>
</div> <!--END HEADER-->

<div id="main"> <!--START MAIN-->

  
  <div id="sidebar"><!--START SIDEBAR-->
    <div id="sidebar_top">&nbsp;</div>

    <?php print $sidebar ?>

  </div><!--END SIDEBAR-->


  <div id="content"> <!--START CONTENT-->

  <?php // print $breadcrumb ?>



  <?php if ($is_front){ ?>
    <h1 class="title">Drupal Themes.org demo website</h1>
  <?php } else { ?>
  <h1 class="title"><?php print $title ?></h1>
  <?php } ?>


  <div class="tabs"><?php print $tabs ?></div>
  <?php if ($show_messages) { print $messages; } ?>
  <?php print $help ?>
  <div id="page_content">
    <?php print $content; ?>
  </div>

  </div> <!--END CONTENT-->

  <div class="clean">&nbsp;</div>

</div> <!-- END MAIN-->


<!--BOTTOM REGION-->
<?php if ($bottom) { ?>
<div id="bottom">
  <?php print $bottom ?>
</div>
<?php } ?>


<?php print $footer ?>


<div id="footer"> <!--START FOOTER-->
  <div id="footer_logos">
    <?php print $footer_message ?>

    <div id="author" > <!-- PLEASE DON'T REMOVE THIS SECTION -->
      Elegant template by <a href="http://www.finex.org">FiNe<strong>X</strong><em>design</em></a> &amp; <a href="http://www.themes-drupal.org">Themes Drupal.org</a>
    </div>

  </div>

</div> <!--END FOOTER-->

</div> <!-- END COLUMN -->

</div> <!--END SHADOW-->

<?php print $closure ?>


</body>
</html>
