  <div class="node<?php if ($sticky) { print " sticky"; } ?><?php if (!$status) { print " node-unpublished"; } ?>">

    <?php  if ($page == 0) { ?>

        <h2><a href="<?php print $node_url ?>" title="<?php print $title ?>"><?php print $title ?></a></h2>
    
    <?php } ?>

    <div class="submitted"><?php print format_date($node->created, 'custom', 'd M Y'); ?>.</div>

    <div class="content"><?php print $content?></div>

    <?php if($node->readmore && ! $page && ! $view ) { print "<span class=\"more\"><a href=\"" . $node_url . "\">" . t('Read more...') . "</a></span>"; } ?>

  </div>
